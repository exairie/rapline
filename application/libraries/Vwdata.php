<?php

/* 
 * Copyright 2016 Exairie
 * Any application should be used with the developer's permission * 
 */

class Vwdata{
    private $data;
    function __construct($params) {
        $this->data = $params["data"];
        get_instance()->load->helper("fields");
    }
    function reinit($data){
        $this->data = $data;
        return $this;
    }
    function get($key){
        if ($this->data == null) return "";
        if (is_array($this->data)){
            if (isset($this->data[$key])) return $this->data[$key];
            
            return "";
        }
        if (is_object($this->data)){
            if(isset($this->data->$key)) return $this->data->$key;
            
            return "";
        }
    }
}