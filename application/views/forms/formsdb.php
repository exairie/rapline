<?php if ($ftype == "data:guru"): ?>
<div class="row">
	<h5>Form Data Guru</h5>
	<div class="col s12">
		<form method="POST">
			<input type="hidden" value="bV9ndXJ1" name="table">
				<div class="input-field">
					<input placeholder="input Nama Guru" id="nama_guru-field" value="<?php echo $d->get("nama_guru") ?>" type="text" name="nama_guru" class="validate">
					<label for="nama_guru-field" class="active">Nama Guru</label>
				</div>
				<div class="input-field">
					<input placeholder="input Kode Identitas" id="kode_identitas-field" value="<?php echo $d->get("kode_identitas") ?>" type="text" name="kode_identitas" class="validate">
					<label for="kode_identitas-field" class="active">Kode Identitas</label>
				</div>
				<div class="input-field">
					<select name="jenis_kelamin" id="" class="browser-default">
						<option value="" disabled="">Pilih Jenis Kelamin</option>
						<option value="L" <?php echo $d->get("jenis_kelamin") == "L"?"selected":"" ?>>Laki-Laki</option>
						<option value="P" <?php echo $d->get("jenis_kelamin") == "P"?"selected":"" ?>>Perempuan</option>
					</select>			
				</div>
				<div class="input-field">
					<input placeholder="input Email" id="email-field" value="<?php echo $d->get("email") ?>" type="email" name="email" class="validate">
					<label for="email-field" class="active">Email</label>
				</div>
			<button class="btn btn-flat">Send</button>
		</form>
	</div>
</div>
<?php elseif($ftype == "data:kelas"): ?>
<div class="row">
	<h5 class="">Input Data Kelas	</h5>
	<div class="col s12">
		<form method="POST">
		<input type="hidden" value="bV9rZWxhcw==" name="table">
			<div class="input-field">
				<input placeholder="input Nama Kelas" id="nama_kelas-field" value="<?php echo $d->get("nama_kelas") ?>" type="text" name="nama_kelas" class="validate">
				<label for="nama_kelas-field" class="active">Nama Kelas</label>
			</div>
			<div class="input-field">
				<input placeholder="input Tahun Masuk" id="tahun_masuk-field" value="<?php echo $d->get("tahun_masuk") ?>" type="number" name="tahun_masuk" class="validate">
				<label for="tahun_masuk-field" class="active">Tahun Masuk</label>
			</div>
			<button class="btn btn-flat">Send</button>
		</form>
	</div>
</div>
<?php elseif($ftype == "data:semester"): ?>
<div class="row">
	<h5 class="">Input Data Semester</h5>
	<div class="col s12">
		<form method="POST">
			<input type="hidden" value="bV9zZW1lc3Rlcg==" name="table">
			<div class="input-field">
				<input placeholder="input Nomor Semester" id="nomor_semester-field" value="<?php echo $d->get("nomor_semester") ?>" type="text" name="nomor_semester" class="validate" required>
				<label for="nomor_semester-field" class="active">Nomor Semester</label>
			</div>
			<div class="input-field">
				<input placeholder="input Titimangsa Rapor" id="titimangsa_rapor-field" value="<?php echo $d->get("titimangsa_rapor") ?>" type="date" name="titimangsa_rapor" class="validate datepicker" required>
				<label for="titimangsa_rapor-field" class="active">Titimangsa Rapor</label>
			</div>
			<div class="input-field">
				<input placeholder="input Tahun Masuk" id="tahun_masuk-field" value="<?php echo $d->get("tahun_masuk") ?>" type="text" name="tahun_masuk" class="validate" required>
				<label for="tahun_masuk-field" class="active">Tahun Masuk</label>
			</div>
			<label for="ganjil-field" class="active" required>Tipe Semester</label>
			<div class="input-field">
				
				<select name="ganjil" id="" class="browser-default">
					<option value="" disabled="">Pilih tipe semester</option>
					<option value="1" <?php echo intval($d->get("ganjil"))==1?"selected":"" ?>>Semester Ganjil</option>
					<option value="0" <?php echo intval($d->get("ganjil"))==0?"selected":"" ?>>Semester Genap</option>
				</select>								
			</div>
			<br>
			<button class="btn btn-flat">Send</button>
		</form>
	</div>
</div>
<?php elseif($ftype == "data:matapelajaran"): ?>
<div class="row">
	<h5 class="">Form Input</h5>
	<div class="col s12">
		<form method="POST">
			<input type="hidden" value="bV9tYXRhX3BlbGFqYXJhbg==" name="table">
			<div class="input-field">
				<input placeholder="input Nama Mata Pelajaran" id="nama_mata_pelajaran-field" value="" type="text" name="nama_mata_pelajaran" class="validate">
				<label for="nama_mata_pelajaran-field" class="active">Nama Mata Pelajaran</label>
			</div>
			<label for="">Kelompok Mata Pelajaran</label>
			<div class="input-field">
				<select name="kelompok" id="" class="browser-default">
					<option value="" disabled>Pilih Kelompok Mata Pelajaran</option>
					<option value="<?php echo MAPEL_KELOMPOK_A ?>">Kelompok A</option>
					<option value="<?php echo MAPEL_KELOMPOK_B ?>">Kelompok B</option>
					<option value="<?php echo MAPEL_KELOMPOK_C ?>">Kelompok C</option>
				</select>							
			</div>
			<br>
			<button class="btn btn-flat">Send</button>
		</form>
	</div>
</div>
<?php endif ?>