<html>
	<head>
		<style>
			#header{
				margin-bottom: 30px;
			}
			#header h3{
				text-align: center;				
			}
			#content{
				position: relative;
				margin: 0px auto;
			}
			#content > table{
				width: 900px;
				margin: 0px auto;				
			}	
			.box{
				border: 1px solid black;
			    height: 150px;
			    padding: 10px;
			    width: 900px;
			}		
			.headerrow td{
				text-align: center;
				background: #ffdf85;
				font-weight: bold;
				/*border: 1px solid #333;*/
			}
			.t-nilai{
				width:100%;
			}
			.t-nilai td{
				padding: 5px;
				border: 1px solid #333;
				min-height: 30px;
			}
			.divider{
				height: 40px;
			}
			.t-absen{
				width: 400px;

			}
			.t-absen td{
				padding: 10px;
				border: 1px solid #222;
			}
			.t-tandatangan{
				width: 100%;
			}
			.t-tandatangan td:nth-child(1){

			}
		</style>
	</head>
	<body>				
		<div id="content">
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td>Nama Sekolah</td>
								<td>:</td>
								<td><?php echo $dsk["nama_sekolah"] ?></td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td>:</td>
								<td><?php echo $dsk["alamat"] ?></td>
							</tr>
							<tr>
								<td>Nama Siswa</td>
								<td>:</td>
								<td><?php echo $ds->nama_siswa ?></td>
							</tr>
							<tr>
								<td>Nomor Induk/NISN</td>
								<td>:</td>
								<td><?php echo $ds->kode_identitas ?></td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td>Kelas</td>
								<td>:</td>
								<td><?php echo $ds->nama_kelas ?></td>
							</tr>
							<tr>
								<td>Semester</td>
								<td>:</td>
								<td><?php echo calculateSmt($ds->tahun_masuk) ?></td>
							</tr>
							<tr>
								<td>Tahun Pelajaran</td>
								<td>:</td>
								<td><?php echo $dtsmt->nomor_semester ?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="divider" colspan="2"></td>
				</tr>
				<tr>
					<td colspan="2"><h4>CAPAIAN HASIL BELAJAR</h4></td>
				</tr>
				<tr>
					<td colspan="2"><b>A. Sikap</b></td>
				</tr>
				<tr>
					<td colspan="2" class="box">						
							<?php echo $cat->deskripsi ?>					
					</td>
				</tr>
				<tr>
					<td colspan="2" class="divider"></td>
				</tr>	
				<tr>
					<td colspan="2">
						<h4>B. Pengetahuan dan Keterampilan</h4>
						<table class="t-nilai" cellspacing="0">
							<tr class="headerrow">
								<td rowspan="2">No</td>
								<td rowspan="2">Mata Pelajaran</td>
								<td colspan="4">Pengetahuan</td>
								<td colspan="4">Keterampilan</td>
							</tr>
							<tr class="headerrow">
								<td>KB</td>
								<td>Angka</td>
								<td>Predikat</td>
								<td>Deskripsi</td>
								<td>KB</td>
								<td>Angka</td>
								<td>Predikat</td>
								<td>Deskripsi</td>
							</tr>
							<tr>
								<td colspan="10"><b>Kelompok A</b></td>
							</tr>
							<?php $n = 0;foreach ($dmp as $mapel): ?>
								<?php if ($mapel->kelompok != MAPEL_KELOMPOK_A) continue; ?>
								<?php  									
									$i = search_where($mapel->id,$dn,"id");

									if($i < 0) {
										?>
										<tr>
											<td colspan="10"><?php echo "search ".$mapel->id.", result = $i"; ?></td>
										</tr>
										<?php
									}

									$data = $dn[$i];
								?>		

								<tr>
									<td><?php echo ++$n ?></td>
									<td><?php echo $mapel->nama_mata_pelajaran ?></td>
									<td><?php echo $data->kkm ?></td>
									<td><?php echo $data->nilai ?></td>
									<td><?php echo predikat($data->nilai)->predikat ?></td>
									<td><?php echo $data->deskripsi_nilai ?></td>
									<td><?php echo $data->kkm ?></td>
									<td><?php echo $data->nilai_keterampilan ?></td>
									<td><?php echo predikat($data->nilai_keterampilan)->predikat ?></td>
									<td><?php echo $data->deskripsi_nilai_keterampilan ?></td>
								</tr>

							<?php endforeach ?>
							<tr>
								<td colspan="10"><b>Kelompok B</b></td>
							</tr>
							<?php $n = 0;foreach ($dmp as $mapel): ?>
								<?php if ($mapel->kelompok != MAPEL_KELOMPOK_B) continue; ?>
								<?php  
									$i = search_where($mapel->id,$dn,"id");
									if($i < 0) continue;

									$data = $dn[$i];
								?>		

								<tr>
									<td><?php echo ++$n ?></td>
									<td><?php echo $mapel->nama_mata_pelajaran ?></td>
									<td><?php echo $data->kkm ?></td>
									<td><?php echo $data->nilai ?></td>
									<td><?php echo predikat($data->nilai)->predikat ?></td>
									<td><?php echo $data->deskripsi_nilai ?></td>
									<td><?php echo $data->kkm ?></td>
									<td><?php echo $data->nilai_keterampilan ?></td>
									<td><?php echo predikat($data->nilai_keterampilan)->predikat ?></td>
									<td><?php echo $data->deskripsi_nilai_keterampilan ?></td>
								</tr>

							<?php endforeach ?>
							<tr>
								<td colspan="10"><b>Kelompok C</b></td>
							</tr><?php $n = 0;foreach ($dmp as $mapel): ?>
								<?php if ($mapel->kelompok != MAPEL_KELOMPOK_C) continue; ?>
								<?php  
									$i = search_where($mapel->id,$dn,"id");
									if($i < 0) continue;

									$data = $dn[$i];
								?>		

								<tr>
									<td><?php echo ++$n ?></td>
									<td><?php echo $mapel->nama_mata_pelajaran ?></td>
									<td><?php echo $data->kkm ?></td>
									<td><?php echo $data->nilai ?></td>
									<td><?php echo predikat($data->nilai)->predikat ?></td>
									<td><?php echo $data->deskripsi_nilai ?></td>
									<td><?php echo $data->kkm ?></td>
									<td><?php echo $data->nilai_keterampilan ?></td>
									<td><?php echo predikat($data->nilai_keterampilan)->predikat ?></td>
									<td><?php echo $data->deskripsi_nilai_keterampilan ?></td>
								</tr>

							<?php endforeach ?>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="divider"></td>
				</tr>
				<tr>
					<td colspan="2">
						<h4>C. Praktik Kerja Lapangan</h4>
						<table class="t-nilai" cellspacing="0">
							<tr class="headerrow">
								<td>No</td>
								<td>Mitra DU/DI</td>
								<td>Lokasi</td>
								<td>Lamanya (Bulan)</td>
								<td>Keterangan</td>
							</tr>							
							<?php $n = 0;foreach ($dpkl as $data): ?>
							<tr>
								<td><?php echo ++$n ?></td>
								<td><?php echo $data->mitra ?></td>
								<td><?php echo $data->lokasi ?></td>
								<td><?php echo $data->durasi ?></td>
								<td><?php echo $data->keterangan ?></td>
							</tr>	
							<?php endforeach ?>
							<?php for($i = count($dpkl) - 1;$i < 3;$i++): ?>
							<tr>
								<td><?php echo ++$n ?></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<?php endfor ?>							
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="divider"></td>
				</tr>
				<tr>
					<td colspan="2">
						<h4>D. Ekstra Kurikuler</h4>
						<table class="t-nilai" cellspacing="0">
							<tr class="headerrow">
								<td>No</td>
								<td>Kegiatan Ekstrakurikuler</td>
								<td>Keterangan</td>
							</tr>
							<?php $n = 0;foreach ($deskul as $data): ?>
							<tr>
								<td><?php echo ++$n ?></td>
								<td><?php echo $data->nama_eskul ?></td>
								<td><?php echo $data->keterangan ?></td>
							</tr>	
							<?php endforeach ?>
							<?php for($i = count($deskul) - 1;$i < 3;$i++): ?>
							<tr>
								<td><?php echo ++$n ?></td>
								<td></td>
								<td></td>
							</tr>
							<?php endfor ?>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="divider"></td>
				</tr>
				<tr>
					<td colspan="2">
						<h4>E. Prestasi</h4>
						<table class="t-nilai" cellspacing="0">
							<tr class="headerrow">
								<td>No</td>
								<td>Jenis Prestasi</td>
								<td>Keterangan</td>
							</tr>
							<?php $n = 0;foreach ($dpres as $data): ?>
							<tr>
								<td><?php echo ++$n ?></td>
								<td><?php echo $data->nama_prestasi ?></td>
								<td><?php echo $data->keterangan ?></td>
							</tr>	
							<?php endforeach ?>
							<?php for($i = count($dpres) - 1;$i < 3;$i++): ?>
							<tr>
								<td><?php echo ++$n ?></td>
								<td></td>
								<td></td>
							</tr>
							<?php endfor ?>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="divider"></td>
				</tr>
				<tr>
					<td colspan="2">
						<h4>F. Kehadiran</h4>
						<table class="t-absen" cellspacing="0">
							<tr>
								<td>Sakit</td>
								<td><?php echo is_null($dabsen)?"0":$dabsen->sakit ?> Hari</td>
							</tr>
							<tr>
								<td>Izin</td>
								<td><?php echo is_null($dabsen)?"0":$dabsen->izin ?> Hari</td>
							</tr>
							<tr>
								<td>Tanpa Keterangan</td>
								<td><?php echo is_null($dabsen)?"0":$dabsen->alfa ?> Hari</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="divider"></td>
				</tr>
				<tr>
					<td colspan="2"><b>G. Catatan Wali Kelas</b></td>
				</tr>				
				<tr>
					<td colspan="2" class="box">						
							<?php echo $cat->cat_sikap ?>					
					</td>
				</tr>
				<tr>
					<td colspan="2" class="divider"></td>
				</tr>
				<tr>
					<td colspan="2"><b>H. Tanggapan Orang Tua/Wali</b></td>
				</tr>				
				<tr>
					<td colspan="2" class="box">						
											
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table class="t-tandatangan">
							<tr>
								<td>
									Mengetahui:	<br>	
									Orang Tua/Wali <br>
									<br>
									<br>
									<br>
									<br>
									................................
								</td>
								<td><br><br><br><br><br>
									Mengetahui:	<br>	
									Kepala Sekolah <br>
									<br>
									<br>
									<br>
									<br>
									NIP.............................
								</td>
								<td>
									<?php echo $dsk["kecamatan"] ?>, <?php echo $dsmt->titimangsa_rapor ?><br>	
									Wali kelas, <?php echo $dwali->nama_guru ?><br>
									<br>
									<br>
									<br>
									<br>
									NIP.............................
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>