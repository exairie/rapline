<html>
	<head>
		<style>
			*{
				font-size: 14px;
			}
			#header{
				margin-bottom: 30px;
			}
			#header h3{
				text-align: center;				
			}
			#content{
				position: relative;
				margin: 0px auto;
			}
			#content table{
				width: 900px;
				margin: 0px auto;				
			}
			#content table td:nth-child(3){
				width: 20px;
			}
			#content table td:nth-child(2){				
				font-weight: bold;
			}
			#content table td{				
				padding: 3px 5px 3px 5px;
			}
		</style>
	</head>
	<body>		
		<div id="header">			
			<h3>KETERANGAN TENTANG DIRI SISWA</h3>
		</div>
		<div id="content">
			<table cellspacing="0">
				<tr>
					<td>1. </td>
					<td>Nama Siswa (Lengkap)</td>
					<td>:</td>
					<td><?php echo $data->nama_siswa ?></td>
				</tr>
				<tr>
					<td>2.</td>
					<td>Nomor Induk/NISN</td>
					<td>:</td>
					<td><?php echo $data->kode_identitas ?></td>
				</tr>
				<tr>
					<td>3.</td>
					<td>Tempat, Tanggal Lahir</td>
					<td>:</td>
					<td><?php echo $data->ttl ?></td>
				</tr>
				<tr>
					<td>4.</td>
					<td>Jenis Kelamin</td>
					<td>:</td>
					<td><?php echo $data->jenis_kelamin ?></td>
				</tr>
				<tr>
					<td>5.</td>
					<td>Agama</td>
					<td>:</td>
					<td><?php echo $data->agama ?></td>
				</tr>
				<tr>
					<td>6.</td>
					<td>Status dalam Keluarga</td>
					<td>:</td>
					<td><?php echo $data->status_dalam_keluarga ?></td>
				</tr>
				<tr>
					<td>7.</td>
					<td>Anak ke</td>
					<td>:</td>
					<td><?php echo $data->anak_ke ?></td>
				</tr>
				<tr>
					<td>8.</td>
					<td>Alamat Siswa</td>
					<td>:</td>
					<td><?php echo $data->alamat_siswa ?></td>
				</tr>
				<tr>
					<td>9.</td>
					<td>Nomor Telepon Rumah</td>
					<td>:</td>
					<td><?php echo $data->no_telp_rumah ?></td>
				</tr>
				<tr>
					<td>10</td>
					<td>Sekolah Asal</td>
					<td>:</td>
					<td><?php echo $data->sekolah_asal ?></td>
				</tr>
				<tr>
					<td>11.</td>
					<td>Diterima di sekolah ini</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td>Di Kelas</td>
					<td>:</td>
					<td><?php echo $data->di_terima_kelas ?></td>
				</tr>
				<tr>
					<td></td>
					<td>Pada Tanggal</td>
					<td>:</td>
					<td><?php echo $data->di_terima_tanggal ?></td>
				</tr>
				<tr>
					<td></td>
					<td>Nama Orang Tua</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td>a. Ayah</td>
					<td>:</td>
					<td><?php echo $data->nama_ayah ?></td>
				</tr>
				<tr>
					<td></td>
					<td>b. Ibu</td>
					<td>:</td>
					<td><?php echo $data->nama_ibu ?></td>
				</tr>
				<tr>
					<td>12.</td>
					<td>Alamat Orang Tua</td>
					<td>:</td>
					<td><?php echo $data->alamat_orangtua ?></td>
				</tr>
				<tr>
					<td></td>
					<td>Nomor Telepon Rumah</td>
					<td>:</td>
					<td><?php echo $data->no_telepon_rumah ?></td>
				</tr>
				<tr>
					<td>13.</td>
					<td>Pekerjaan Orang Tua</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td>a. Ayah</td>
					<td>:</td>
					<td><?php echo $data->pekerjaan_ayah ?></td>
				</tr>
				<tr>
					<td></td>
					<td>b. Ibu</td>
					<td>:</td>
					<td><?php echo $data->pekerjaan_ibu ?></td>
				</tr>
				<tr>
					<td>14.</td>
					<td>Nama Wali Siswa</td>
					<td>:</td>
					<td><?php echo $data->nama_wali ?></td>
				</tr>
				<tr>
					<td>15.</td>
					<td>Alamat Wali Siswa</td>
					<td>:</td>
					<td><?php echo $data->alamat_wali ?></td>
				</tr>
				<tr>
					<td></td>
					<td>Nomor Telpon Rumah</td>
					<td>:</td>
					<td><?php echo $data->no_telepon_wali ?></td>
				</tr>
				<tr>
					<td>16.</td>
					<td>Pekerjaan Wali Siswa</td>
					<td>:</td>
					<td><?php echo $data->pekerjaan_wali ?></td>
				</tr>
			</table>
		</div>
	</body>
</html>