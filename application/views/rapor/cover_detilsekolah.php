<html>
	<head>
		<style>
			*{
				font-size: 14px;
			}
			#header{
				margin-bottom: 30px;
			}
			#header h3{
				text-align: center;				
			}
			#content{
				position: relative;
				margin: 0px auto;
			}
			#content table{
				width: 900px;
				margin: 0px auto;				
			}
			#content table td:nth-child(2){
				width: 20px;
			}
			#content table td:first-child{				
				font-weight: bold;
			}
			#content table td{				
				padding: 15px;
			}
		</style>
	</head>
	<body>		
		<div id="header">
			<h3>RAPOR SISWA</h3>
			<h3>SEKOLAH MENENGAH KEJURUAN (SMK)</h3>
		</div>
		<div id="content">
			<table cellspacing="0">
				<tr>
					<td>Nama Sekolah</td>
					<td>:</td>
					<td><?php echo $nama_sekolah ?></td>
				</tr>
				<tr>
					<td>NPSN</td>
					<td>:</td>
					<td><?php echo $npsn ?></td>
				</tr>
				<tr>
					<td>NIS/NSS/NDS</td>
					<td>:</td>
					<td><?php echo $nis_nss_nds ?></td>
				</tr>
				<tr>
					<td>Alamat Sekolah</td>
					<td>:</td>
					<td><?php echo $alamat ?> <br><b>Kode Pos : </b><?php echo $kode_pos ?> <b>Telp: </b><?php echo $telepon ?></td>
				</tr>
				<tr>
					<td>Kelurahan</td>
					<td>:</td>
					<td><?php echo $kelurahan ?></td>
				</tr>
				<tr>
					<td>Kecamatan</td>
					<td>:</td>
					<td><?php echo $kecamatan ?></td>
				</tr>
				<tr>
					<td>Kota/Kabupaten</td>
					<td>:</td>
					<td><?php echo $kota_kabupaten ?></td>
				</tr>
				<tr>
					<td>Provinsi</td>
					<td>:</td>
					<td><?php echo $provinsi ?></td>
				</tr>
				<tr>
					<td>Website</td>
					<td>:</td>
					<td><?php echo $website ?></td>
				</tr>
				<tr>
					<td>E-Mail</td>
					<td>:</td>
					<td><?php echo $email ?></td>
				</tr>
			</table>
		</div>
	</body>
</html>